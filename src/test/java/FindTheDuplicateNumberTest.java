import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FindTheDuplicateNumberTest {

    @Test
    @DisplayName("[1,2] returns -1")
    public void noDuplicateShouldReturnMinusOne(){
        int duplicate = FindTheDuplicateNumber.findDuplicate(new int[]{1, 2});
        assertEquals(-1, duplicate);
    }

    @Test
    @DisplayName("[1,1] returns 1")
    public void onlyTwoNumbersDuplicateShouldReturnTheNumber(){
        int duplicate = FindTheDuplicateNumber.findDuplicate(new int[]{1, 1});
        assertEquals(1, duplicate);
    }

    @Test
    @DisplayName("[1,2,1] returns 1")
    public void treeNumbersWithDuplicateShouldReturnTheNumber(){
        int duplicate = FindTheDuplicateNumber.findDuplicate(new int[]{1, 2, 1});
        assertEquals(1, duplicate);
    }

}