package leetcode;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidParenthesesTest {

    ValidParentheses validParentheses = new ValidParentheses();

    @Test
    @DisplayName(" Receiving a regular open/close parentheses () should return true")
    public void singleParentheses() {

        boolean valid = validParentheses.isValid("()");
        assertTrue(valid);
    }

    @Test
    @DisplayName(" Receiving multiple regular open/close ()[]{} should return true")
    public void multipleRegularParentheses() {
        boolean valid = validParentheses.isValid("()[]{}");
        assertTrue(valid);
    }

    @Test
    @DisplayName(" Receiving wrong open/close (] should return false")
    public void wrongParentheses() {
        boolean valid = validParentheses.isValid("(]");
        assertTrue(!valid);
    }




}