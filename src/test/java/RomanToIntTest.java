import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RomanToIntTest {

    RomanToInt romanToInt = new RomanToInt();

    @Test
    public void one(){
        int result = romanToInt.romanToInt("I");
        assertEquals(1,result);
    }

    @Test
    public void two(){
        int result = romanToInt.romanToInt("II");
        assertEquals(2,result);
    }

    @Test
    public void Three(){
        int result = romanToInt.romanToInt("III");
        assertEquals(3,result);
    }

    @Test
    public void four(){
        int result = romanToInt.romanToInt("IV");
        assertEquals(4,result);
    }

    @Test
    public void six(){
        int result = romanToInt.romanToInt("VI");
        assertEquals(6,result);
    }

    @Test
    public void fourteen(){
        int result = romanToInt.romanToInt("XIV");
        assertEquals(14,result);
    }

    @Test
    public void sixteen(){
        int result = romanToInt.romanToInt("XVI");
        assertEquals(16,result);
    }

    @Test
    public void fiftyeight(){
        int result = romanToInt.romanToInt("LVIII");
        assertEquals(58,result);
    }


    @Test
    public void nineteenninetyfour(){
        int result = romanToInt.romanToInt("MCMXCIV");
        assertEquals(1994,result);
    }








}