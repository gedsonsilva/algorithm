import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoSumTest {

    @Test
    void twoSumPositiveNumbers() {
        TwoSum ts = new TwoSum();
        int[] ints = {2, 7, 11, 15};
        int[] result = ts.twoSum(ints, 9);
        assertEquals(1,result[0] + result[1]);
    }

    @Test
    void twoSumNegativeNumbers() {
        TwoSum ts = new TwoSum();
        int[] ints = {15, 7, -3, 15};
        int[] result = ts.twoSum(ints, 4);
        assertEquals(3,result[0] + result[1]);
    }

}