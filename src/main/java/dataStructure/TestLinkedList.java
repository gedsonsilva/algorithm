package dataStructure;

public class TestLinkedList {

    public static void main(String[] args) {
        LinkedList list =  new LinkedList();
        list.addLast(1);
        list.addLast(2);
        System.out.println(list);

        java.util.LinkedList jList = new java.util.LinkedList();
        jList.addLast(1);
        System.out.println(jList);
    }
}
