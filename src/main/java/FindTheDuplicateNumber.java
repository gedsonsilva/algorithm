import java.util.Arrays;

/**
 * https://prepfortech.in/interview-topics/arrays/find-the-duplicate-number
 */
public class FindTheDuplicateNumber {

    public static int findDuplicate(int[] nums) {
        Arrays.sort(nums); //TODO - IMPLEMENT THE SORT METHOD
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                return nums[i];
            }
        }
        return -1;
    }
}
