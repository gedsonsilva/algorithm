/**
 * https://algocademy.com/app/#problem/binary-strings-with-at-most-k-consecutive-ones/lang/java/solution/1-10/tab/solution-hints/noredirect/true
 */
public class MostConsecutive {

    public static void main(String[] args) {
        int n = 4, k=2;

        int value = countBinaryStrings(n,k);
        System.out.println(value);
    }

    public static int countBinaryStrings(int n, int k) {
        int[][] dp = new int[n + 1][k + 1];
        dp[0][0] = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= k; j++) {
                dp[i + 1][0] += dp[i][j];
                if (j < k) {
                    dp[i + 1][j + 1] += dp[i][j];
                }
            }
        }
        int result = 0;
        for (int j = 0; j <= k; j++) {
            result += dp[n][j];
        }
        return result;
    }
}
