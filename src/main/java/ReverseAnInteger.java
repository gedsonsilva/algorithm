public class ReverseAnInteger {

    public static void main(String[] args) {
        int input = 654321;
        int result = 0;
        while(input != 0){
            result = result * 10 + input % 10 ;
            input = input / 10 ;
        }
        System.out.println(result);

    }
}
