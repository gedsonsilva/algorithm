package books.algorithmsNotes;

public class FindAMaxValue {

    public int findMax(int[] arr){
        int max = Integer.MIN_VALUE;
        for(int i = 0 ; i < arr.length ; i++){
            if(arr[i] > max){
                max = arr[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        FindAMaxValue findAMaxValue = new FindAMaxValue();
        int[] input = {100,1,3,4,5,8,322,34,78,64};
        System.out.println(findAMaxValue.findMax(input));
    }
}
