package books.algorithmsNotes;

public class FIzzBuzz {

    static int[] input = {45, 1, 2, 3, 4, 5, 6, 7, 8, 10, 15};

    public void printFizzBuzz(int[] input) {
        for (int n : input) {
            if (n % 15 == 0){
                System.out.println("FIZZ BUZZ");
            } else if(n % 3 == 0){
                System.out.println("FIZZ");
            }else if(n % 5 == 0){
                System.out.println("BUZZ");
            }else {
                System.out.println(n);
            }
        }
    }

    public static void main(String[] args) {
        FIzzBuzz fIzzBuzz = new FIzzBuzz();
        fIzzBuzz.printFizzBuzz(input);
    }
}
