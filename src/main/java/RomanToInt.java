import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RomanToInt {

    public int romanToInt(String s) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);

        char[] number = s.toCharArray();

        int result = map.get(number[number.length - 1]);
        for (int i = number.length - 2; i >= 0; i--) {
            if (map.get(number[i]) >= map.get(number[i + 1])) {
                result += map.get(number[i]);
            } else {
                result -= map.get(number[i]);
            }

        }
        return Math.abs(result);
    }
}
