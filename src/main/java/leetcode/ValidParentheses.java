package leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * https://leetcode.com/problems/valid-parentheses/
 */

public class ValidParentheses {

    public boolean isValid(String s) {
        if (s.length() == 1) return false;

        Stack<Character> stack = new Stack();

        char[] chars = s.toCharArray();
        for(Character c : chars){
            if(!isTerminator(c)){
                stack.push(c);
            }else{
                if(stack.isEmpty()){
                    return false;
                }else{
                    char pop = stack.pop();
                    if(c == '}' && pop != '{'){
                        return false;
                    }else if(c == ')' && pop != '('){
                        return false;
                    }else if(c == ']' && pop != '['){
                        return false;
                    }
                }
            }
        }
        return stack.empty();
    }

    private boolean isTerminator(char s) {
        if(s=='}' || s==')'|| s==']') return true;
        return false;
    }
}
