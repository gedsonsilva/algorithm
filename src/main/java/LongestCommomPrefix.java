/**
 * https://leetcode.com/problems/longest-common-prefix/
 */
public class LongestCommomPrefix {

    public static void main(String[] args) {
        String[] words = new String[]{"Flowers", "Flow", "Flight"};
        String s = longestCommonPrefix(words);
        System.out.println(s);

        words = new String[]{"Flowers", "cat", "Titinac"};
        s = longestCommonPrefix(words);
        System.out.println(s);
    }

    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        String prefix = strs[0];
        for (int i = 1; i < strs.length; i++) {
            while (strs[i].indexOf(prefix) != 0) {
                prefix = prefix.substring(0, prefix.length() -1);
            }
        }
        return prefix;
    }

}
