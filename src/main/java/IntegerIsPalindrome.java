import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/palindrome-number/submissions/
 */
public class IntegerIsPalindrome {

    public static void main(String[] args) {
        var x = new IntegerIsPalindrome();
        boolean palindrome = x.isPalindrome(1221);
        boolean palindrome1 = x.isPalindrome(12321);
        boolean palindrome2 = x.isPalindrome(12341);
    }

    public boolean isPalindrome(int x) {
        if (x < 0 ) {
            return false;
        }
        int reversedNumber = 0, check = x;
        while (x != 0 ) {
            reversedNumber = reversedNumber * 10 + x % 10;
            x /= 10;
        }

        return check == reversedNumber ;
    }
}
